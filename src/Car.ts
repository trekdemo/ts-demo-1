export class Car {
  name: string;
  speed: number;

  constructor(name: string) {
    this.name = name;
    this.speed = Math.random() * 10;
  }

  toString(): string {
    return `Car name=(${this.name}) speed=(${this.speed})`;
  }
};

export class Bus extends Car {
}

export interface Vehicle {
  name: string;
  speed: number;
};
