import 'phaser'

export class TestScene extends Phaser.Scene {
  cursors: any;
  rect?: Phaser.GameObjects.Group;

  constructor() {
    super({key: 'TestScene'});
  }

  preload() { }
  create() {
    this.cursors = this.input.keyboard.createCursorKeys();
    let rect = this.add.graphics();
    rect.lineStyle(2, 0xffffff, 1);
    rect.fillStyle(0xffffff, 1);
    rect.fillRect(0, 0, 50, 50);
    this.rect = this.add.group([rect]);
  }
  update(time: number, delta:number) {
    if (this.cursors.left.isDown)
      console.log('Left');
    if (this.cursors.right.isDown)
      console.log('Right');
    if (this.cursors.up.isDown)
      console.log('Up');
    if (this.cursors.down.isDown)
      console.log('Down');
  }
}
