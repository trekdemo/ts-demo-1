const path = require('path');
const webpack = require('webpack');

// Phaser webpack config
const phaserModule = path.join(__dirname, '/node_modules/phaser/')
const phaser = path.join(phaserModule, 'src/phaser.js')

module.exports = {
  entry: './src/index.ts',
  // entry: {
  //   app: './src/index.ts',
  //   vendor: ['phaser'],
  // },
  output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
  },
  mode: 'development',
  // mode: 'production',
  devtool: "source-map",
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      'phaser': phaser,
    }
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'ts-loader'
      }
    ]
  },
};
